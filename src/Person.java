
public class Person {
	
	private static int cnt = 0;
	
	private final int ID = cnt++;
	private String name;
	private String gender;
	private String nationality;
	
	public Person() {
		name = "unspecified";
		gender = "unspecified";
		nationality = "unspecified";
	}
	
	public Person(String name, String gender, String nationality) {
		this.name = name;
		this.gender = gender;
		this.nationality = nationality;
	}
	
	public int getID() {
		return ID;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public String getGender() {
		return this.gender;
	}
	
	public void setGender(String newGender) {
		
		if (!newGender.equals("F") && !newGender.equals("M")) {
			try {
				throw new GenderFormatException("Please input valid gender format");
			} catch (GenderFormatException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		} 
		else this.gender = newGender;
	}
	
	public String getNationality() {
		return this.nationality;
	}
	
	public void setNationality(String newNationality) {
		this.nationality = newNationality;
	}
	
	public String toString() {
		return "ID: " + ID + "\n" +
				"Name: " + name + "\n" +
				"Gender: " + gender + "\n" +
				"Nationality: " + nationality + "\n";
	}
	
	public static void main(String[] args) {
		Person p1 = new Person();
		Person p2 = new Person("Jerry", "M", "Korean");
		Person p3 = new Person("Cheung", "M", "Chinese");
		
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
		p1.setName("Mary");
		p1.setGender("F");
		p1.setNationality("American");
		System.out.println(p1);
		
		p1.setGender("Female");
		System.out.println(p1);
	}

	public int compareTo(Student s) {
		// TODO Auto-generated method stub
		return 0;
	}
}
