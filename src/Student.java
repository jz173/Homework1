import java.sql.Date;
import java.util.Arrays;
import java.util.Comparator;

public class Student extends Person implements Comparable{
	private static int studCnt = 0;
	
	private final int studentID = studCnt++;
	private double GPA;
	private Date gradDate;
	
	@SuppressWarnings("deprecation")
	public Student() {
		super();
		GPA = 0.0;
		gradDate = new Date(0, 0, 1);
	}
	
	public Student(String name, String gender, String nationality, double GPA, Date date) {
		super(name, gender, nationality);
		this.GPA = GPA;
		this.gradDate = date;
	}
	
	public int getstudID() {
		return studentID;
	}
	
	public double getGPA() {
		return GPA;
	}
	
	public void setGPA(double newGPA) {
		this.GPA = newGPA;
	}
	
	public String getGradDate() {
		return gradDate.toString();
	}
	
	public void setGradDate(Date date) {
		this.gradDate = date;
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Student s = (Student) o;
		if (GPA == s.GPA) return 0;
		return GPA - s.GPA < 0 ? -1 : 1;
	}
	
	public String toString() {
		return super.toString() + 
				"studentID: " + studentID + "\n" +
				"GPA: " + GPA + "\n" +
				"Graduation date: " + gradDate + "\n";
	}
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		Student s1 = new Student();
		Student s2 = new Student("Oliver", "M", "Japanese", 3.0, new Date(115, 6, 6));
		Student s3 = new Student("Jane", "F", "Indian", 3.5, new Date(114, 8, 8));
		Student s4 = new Student("Scott", "M", "Chinese", 4.0, new Date(116, 11, 30));
		
//		System.out.println(s1);
//		System.out.println(s2);
//		System.out.println(s3);
//		System.out.println(s4);
		
		Person p1 = s1;
		Person p2 = s2;
		Person p3 = s3;
		Person p4 = s4;
		Person p5 = new Student("Jerry", "M", "Chinese", 3.9, new Date(116, 11, 30));
//		System.out.println(p1);
//		System.out.println(p2);
//		System.out.println(p3);
//		System.out.println(p4);
//		System.out.println(p5);
		
		Student[] arr = new Student[4];
		arr[0] = s4;
		arr[1] = s3;
		arr[2] = s2;
		arr[3] = s1;
//		for (Person p : pArrs) {
//			System.out.println(p);
//		}
		
		Arrays.sort(arr);
		for (Student s : arr) {
			System.out.println(s);
		}
	}
}
