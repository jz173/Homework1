
public class GenderFormatException extends Exception{
	
	public GenderFormatException(String msg) {
		super(msg);
	}
}
